
#include "Common.h"

CSkyDome 			Sky;
CCamera 			Camera;
CFrustum 			Frustum;
VolumetricClouds 	Clouds;

SDL_Surface*	MainWindow;

int 	ScreenBpp = 32;
int 	ScreenDepth = 24;
int 	ScreenHeight = 600;
int 	ScreenWidth = 800;
SDL_TimerID timerId = 0;

float 	CloudPlaneSize = 400.0f;
float 	CloudPlaneHeight = 100.0f;
float 	SkyRadius = 200.0f;

bool	fullscreen = false;

// Our transition function for cellular automata
Uint32 transition(Uint32 interval, void *params) {
	// update our frames
	glLoadIdentity();				
	Camera.Update();
	Camera.Look();
	Frustum.CalculateFrustum();	

	Vector3 sky = Sky.GetSunVector() * SkyRadius;
	Clouds.Transition(Sky.GetSunVector() * SkyRadius, Camera.GetCameraPosition());

	glClearColor(0.0f, 0.8f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	return 1000;
}

int init_game() {

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	SDL_GL_SwapBuffers();

	// Setup our environment
	Camera.Create(45.0f, ScreenWidth, ScreenHeight, 4096.0f);
	if (Sky.Initialize(SkyRadius, 24, 32, false)) return 1;	
	if (Clouds.Create(5, CloudPlaneSize / 2, CloudPlaneHeight)) return 1;
	Camera.PositionCamera(0, 0, 0, Sky.GetSunVector().x * 10, Sky.GetSunVector().y * 10, Sky.GetSunVector().z * 10, 0, 1, 0);

	SDL_ShowCursor(SDL_DISABLE);

	return 0;
}

int update_frame() {		

	// update our frames
	glLoadIdentity();				
	Camera.Update();
	Camera.Look();
	Frustum.CalculateFrustum();	

	Vector3 sky = Sky.GetSunVector() * SkyRadius;
	Clouds.Update(Sky.GetSunVector() * SkyRadius, Camera.GetCameraPosition());
	
	// render sky and clouds
	glClearColor(0.0f, 0.8f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0f, 1.0f, 1.0f);
		
	Sky.Render(Camera.GetCameraPosition());
	Clouds.Render(Camera.GetCameraPosition(), Sky.GetSunVector() * SkyRadius);

	// Add our timer if it does not already exist
	if (timerId == 0) {
		timerId = SDL_AddTimer(1000, transition, NULL);
	}
	
	SDL_GL_SwapBuffers();
	
	return 0;
}

void terminate_game() {		
	Sky.Destroy();
}

int init_opengl() {	
	glRenderMode(GL_RENDER);
	glClearColor(0.0f, 0.8f, 1.0f, 0.0f);
	glDepthFunc(GL_LEQUAL);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	return 0;
}

int set_display_mode(int width, int height, int bpp, bool fullscreen) {
	unsigned int flags = SDL_OPENGL | SDL_HWSURFACE;  // | SDL_RESIZABLE;

	if (fullscreen) 	flags |= SDL_FULLSCREEN;

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, ScreenDepth);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 0);
	SDL_GL_SetAttribute(SDL_GL_ACCUM_RED_SIZE, 0);
	SDL_GL_SetAttribute(SDL_GL_ACCUM_GREEN_SIZE, 0);
	SDL_GL_SetAttribute(SDL_GL_ACCUM_BLUE_SIZE, 0);
	SDL_GL_SetAttribute(SDL_GL_ACCUM_ALPHA_SIZE, 0);
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

	if (MainWindow)
		SDL_FreeSurface(MainWindow);

	MainWindow = SDL_SetVideoMode(width, height, bpp, flags);

	if (!MainWindow) {
		cout << "INIT: Cannot initialise video mode or create window in " << width << "x" << height << "x" << bpp << " DepthSize: " << ScreenDepth << " " << endl;
		return 1;
	}
	
	cout << "INIT: Video mode set to " << width << "x" << height << "x" << bpp << " DepthSize: " << ScreenDepth << " " << endl;
	
	return 0;
}

#ifdef WIN32
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
#else
int main(int argc, char *argv[])
#endif
{		
	cout << "Initializing Skydome demo" << endl;
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) <  0) {
		cout << "Could not initialize SDL:" << SDL_GetError() << endl;
		SDL_Quit();
		return 1;
	} 
	
	cout << "SDL Initialization successful" << endl;

	if (set_display_mode(ScreenWidth, ScreenHeight, ScreenBpp, fullscreen)) {
		SDL_Quit();
		return 1;
	}
		
	init_opengl();
	ilInit();
	
	if (init_game()) {
		ilShutDown();
		SDL_Quit();
		return 1;
	}
	
	bool running = true;
	SDL_Event event;
	
	while (running) {
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
				case SDL_VIDEORESIZE:
					ScreenWidth = event.resize.w;
					ScreenHeight = event.resize.h;					
					set_display_mode(ScreenWidth, ScreenHeight, ScreenBpp, false);		
					break;
					
				case SDL_QUIT:
					running = false;
					break;
				case SDL_KEYDOWN:
					if (event.key.keysym.sym == SDLK_ESCAPE) {
							running = false;
					}
					break;
			}			
		}	
		if (update_frame())
			break;		
	}	
	terminate_game();
	ilShutDown();
	SDL_Quit();
	return 0;
}
