#include "Common.h"

#define GL_CLAMP_TO_EDGE 0x812F

VolumetricClouds::VolumetricClouds() {
	Albedo = 0.95f;
	Extinction = 80.0f;
	SplatBufferSize = 128;		// Splat Buffer size
	ImpostorSize = 512;			
}

void VolumetricClouds::GrowCloud(VolumetricCloud* Cloud) {
	int Num;

	Cloud->BoundingBox1 = Vector3(0, 0, 0);
	Cloud->BoundingBox2 = Vector3(XDIM, YDIM, ZDIM);
	
	//allocate buffers for rendering
	Num = XDIM * YDIM * ZDIM;
	Cloud->VertexBuffer = new Vector3[Num * 4];
	Cloud->TexCoordBuffer = new Vector2[Num * 4];
	Cloud->ColorBuffer = new Color4[Num * 4];
}

void VolumetricClouds::GenerateTexture() {
	int N = 64;
	unsigned char *B = new unsigned char[4*N*N];
	float X,Y,Dist;
	float Incr = 2.0f/N;
	int i = 0, j = 0;
	float value;

	Y = -1.0f;
	for (int y = 0; y < N; y++) {
		X = -1.0f;
		for (int x=0; x<N; x++, i++, j+=4) {
			Dist = (float)sqrt(X*X+Y*Y);
			if (Dist > 1) Dist=1;

			value = 2*Dist*Dist*Dist - 3*Dist*Dist + 1;
			value *= 0.4f;

			B[j+3] = B[j+2] = B[j+1] = B[j] = (unsigned char)(value * 255);
			
			X+=Incr;
		}
		Y+=Incr;
	}

	glGenTextures(1, &PuffTexture);
	glBindTexture(GL_TEXTURE_2D, PuffTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, N, N, 0, GL_RGBA, GL_UNSIGNED_BYTE, B);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	

	delete [] B;		
}

int VolumetricClouds::Create(int Num, float PlaneSize, float PlaneHeight) {
	int i;
	
	// Create our texture using our interpolation polynomial
	GenerateTexture();

	for (i = 0; i < Num; i++)
	{
		VolumetricCloud Cloud;
		// Initialize cellular automata grids
		for (int x = 0; x < XDIM; x++) {
			for (int y = 0; y < YDIM; y++) {
				Cloud.cld[x][y] = 0;
				Cloud.hum[x][y] = (rand() << 16) + rand();
				Cloud.act[x][y] = (rand() << 16) + rand();
			}
		}
		// Generates cloud with randomized x and z coordinates and given y coordinate
		Cloud.Center.x = rand() % (int)(PlaneSize * 2) - PlaneSize;
		Cloud.Center.z = rand() % (int)(PlaneSize * 2) - PlaneSize;
		Cloud.Center.y = PlaneHeight;
		// Radius based on our dimensions set
		Cloud.Radius = XDIM + YDIM + ZDIM;
		// Initialize the lighting and camera positions
		Cloud.LastLight = Vector3(0, 0, 0);
		Cloud.LastCamera = Vector3(0, 0, 0);
		Cloud.ImpostorTex = 0;
		
		GrowCloud(&Cloud);

		Clouds.push_back(Cloud);
	}
	
	return 0;
}

void VolumetricClouds::Transition(Vector3 Sun, Vector3 Camera) {
	Vector3 SunDir, ToCam;
	SunDir = Normalize(Sun);
	for (unsigned i = 0; i < Clouds.size(); i++) {
		ToCam = Normalize(Camera - Clouds[i].Center);
		for (int x = 0; x < XDIM; x++) {
			for (int y = 0; y < YDIM; y++) {
				// Bitwise transition functions as explained in paper
				int comp = (((rand() << 16) + rand()) | ((rand() << 16) + rand()) | ((rand() << 16) + rand()));
				Clouds[i].hum[x][y] = Clouds[i].hum[x][y] & ~Clouds[i].act[x][y];
				int fact = (Clouds[i].act[x][y] << 1) | (Clouds[i].act[x][y] << 2) |
					(Clouds[i].act[x][y] >> 1) | (Clouds[i].act[x][y] >> 2) |
					(Clouds[i].act[x][y] << 1);
				if (x + 1 < XDIM) { fact |= (Clouds[i].act[x + 1][y]); }
				if (x + 2 < XDIM) { fact |= (Clouds[i].act[x + 2][y]); }
				if (x - 1 >= 0) { fact |= (Clouds[i].act[x - 1][y]); }
				if (x - 2 >= 0) { fact |= (Clouds[i].act[x - 2][y]); }
				if (x + 1 < XDIM) { fact |= (Clouds[i].act[x + 1][y]); }
				if (y + 1 < YDIM) { fact |= (Clouds[i].act[x][y + 1]); }
				if (y - 1 >= 0) { fact |= (Clouds[i].act[x][y - 1]); }
				if (y - 2 >= 0) { fact |= (Clouds[i].act[x][y - 2]); }
				Clouds[i].act[x][y] = ~Clouds[i].act[x][y] & Clouds[i].hum[x][y] & fact;
				if (rand() % 5 == 1) {
					Clouds[i].cld[x][y] = Clouds[i].cld[x][y] & comp;
				}
				Clouds[i].cld[x][y] = Clouds[i].cld[x][y] | Clouds[i].act[x][y];
				Clouds[i].hum[x][y] = Clouds[i].hum[x][y] | comp;
				Clouds[i].act[x][y] = Clouds[i].act[x][y] | comp;
			}
		}
		MakeCloudBillboard(&Clouds[i], Sun, Camera);
		Clouds[i].LastLight = SunDir;
		Clouds[i].LastCamera = Camera;
	}
}
void VolumetricClouds::Update(Vector3 Sun, Vector3 Camera) {
	Vector3 SunDir, ToCam;

	SunDir = Normalize(Sun);

	for (unsigned i = 0; i < Clouds.size(); i++)
	{
		//  if the angle between the camera and the cloud center
		//  has changed enough since the last lighting calculation
		//  recalculate the lighting 
		ToCam = Normalize(Camera - Clouds[i].Center);
		if (Dot(SunDir, Clouds[i].LastLight) < 0.99f) {
			LightCloud(&Clouds[i], Sun);
			MakeCloudBillboard(&Clouds[i], Sun, Camera);
			Clouds[i].LastLight = SunDir;
			Clouds[i].LastCamera = ToCam;
		} else {
			float dot = Dot(ToCam, Clouds[i].LastCamera);
			bool in_frustum = Frustum.SphereInFrustum(Clouds[i].Center, Clouds[i].Radius);

			//same as above only recreating the impostor
			if ((dot < 0.99f) && in_frustum) {
				MakeCloudBillboard(&Clouds[i], Sun, Camera);
				Clouds[i].LastCamera = ToCam;
			}
		}
	}
}

void VolumetricClouds::Render(Vector3 Camera, Vector3 Sun) {
	unsigned i, j;

	for (i = 0; i < Clouds.size(); i++) {
		Clouds[i].DistanceFromCamera = SqDist(Clouds[i].Center, Camera);
	}

	// we sort our clouds before rendering
	bool done = false;
	while (!done) {
		done = true;
		for (i = 0; i < Clouds.size(); i++)
			for (j = i + 1; j < Clouds.size(); j++) {
				if (Clouds[i].DistanceFromCamera < Clouds[j].DistanceFromCamera) {
					swap(Clouds[i], Clouds[j]);
					done = false;
				}
			}
	}

	for (i = 0; i < Clouds.size(); i++) {
		RenderCloudBillboard(&Clouds[i], 1.0f);
	}
}

void VolumetricClouds::MakeCloudBillboard(VolumetricCloud* Cloud, Vector3 Sun, Vector3 Camera) {
	int 	ViewportSize = ImpostorSize;

	float d = Dist(Camera, Cloud->Center);
	float r = Cloud->Radius;
	float pr = 2.0f;

	ViewportSize = ImpostorSize;

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(-Cloud->Radius - pr, Cloud->Radius + pr, -Cloud->Radius - pr, Cloud->Radius + pr, d - r, d + r);

	//we setup the camera to look at the cloud center from the camera position
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	gluLookAt(Camera.x, Camera.y, Camera.z, Cloud->Center.x, Cloud->Center.y, Cloud->Center.z, 0, 1, 0);

	glPushAttrib(GL_VIEWPORT_BIT);
	glViewport(0, 0, ViewportSize, ViewportSize);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	float mat[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, mat);

	Vector3 vx(mat[0], mat[4], mat[8]);
	Vector3 vy(mat[1], mat[5], mat[9]);

	Cloud->vx = vx;		Cloud->vy = vy; //store for rendering

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, PuffTexture);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

	Vector3 Light = Normalize(Camera - Sun);
	Vector3 Omega;
	Color4  ParticleColor;

	int index = 0;

	Vector3* vp; Vector2* tp; Color4* cp;

	int Num = XDIM * YDIM * ZDIM;

	vp = Cloud->VertexBuffer;
	tp = Cloud->TexCoordBuffer;
	cp = Cloud->ColorBuffer;

	Vector2 v1(1.0f, 0.0f), v2(0.0f, 0.0f), v3(0.0f, 1.0f), v4(1.0f, 1.0f);
	float costheta, phase;
	float omega2;

	Vector3 Corner1, Corner2, Corner3, Corner4;
	Corner1 = -vx - vy; Corner2 = vx - vy; Corner3 = vx + vy; Corner4 = vy - vx;

	for (int x = 0; x < XDIM; x++) {
		for (int y = 0; y < YDIM; y++) {
			for (int z = 0; z < ZDIM; z++) {
				// Does cld at point exist
				if (Cloud->cld[x][y] & (1 << z)) {
					Vector3 pos = Vector3(x, y, z) + Cloud->Center;
					Omega = pos - Camera;

					omega2 = Omega.x * Omega.x + Omega.y * Omega.y + Omega.z * Omega.z;
					omega2 = carmack_func(omega2);

					Omega.x *= omega2;	Omega.y *= omega2;	Omega.z *= omega2;

					costheta = Dot(Omega, Light);
					phase = 0.75f * (1.0f + costheta * costheta);

					ParticleColor.R = 0.3f + Cloud->clr[x][y][z].R * phase;
					ParticleColor.G = 0.3f + Cloud->clr[x][y][z].G * phase;
					ParticleColor.B = 0.3f + Cloud->clr[x][y][z].B * phase;
					ParticleColor.A = Cloud->clr[x][y][z].A * 1.0f;

					*(vp++) = pos + Corner1 * 2.0f;
					*(tp++) = v1;
					*(cp++) = ParticleColor;

					*(vp++) = pos + Corner2 * 2.0f;
					*(tp++) = v2;
					*(cp++) = ParticleColor;

					*(vp++) = pos + Corner3 * 2.0f;
					*(tp++) = v3;
					*(cp++) = ParticleColor;

					*(vp++) = pos + Corner4 * 2.0f;
					*(tp++) = v4;
					*(cp++) = ParticleColor;

					ParticleColor = ParticleColor * 1.5f;
					ParticleColor.Clamp();

					glColor4fv(!ParticleColor);

					glBegin(GL_QUADS);

					glTexCoord2f(1.0f, 0.0f);
					glVertex3fv(!(pos + (vx + vy) * -2.0f));
					glTexCoord2f(0.0f, 0.0f);
					glVertex3fv(!(pos + (vx - vy) *  2.0f));
					glTexCoord2f(0.0f, 1.0f);
					glVertex3fv(!(pos + (vx + vy) *  2.0f));
					glTexCoord2f(1.0f, 1.0f);
					glVertex3fv(!(pos + (vy - vx) *  2.0f));

					glEnd();
				} else {
					Vector3 nopos = Vector3(0, 0, 0);
					Color4 color = Color4(0, 0, 0, 0);

					*(vp++) = nopos;
					*(tp++) = v1;
					*(cp++) = color;

					*(vp++) = nopos;
					*(tp++) = v2;
					*(cp++) = color;

					*(vp++) = nopos;
					*(tp++) = v3;
					*(cp++) = color;

					*(vp++) = nopos;
					*(tp++) = v4;
					*(cp++) = color;
				}
			}
		}
	}

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	glVertexPointer(3, GL_FLOAT, 0, Cloud->VertexBuffer);
	glColorPointer(4, GL_FLOAT, 0, Cloud->ColorBuffer);
	glTexCoordPointer(2, GL_FLOAT, 0, Cloud->TexCoordBuffer);

	glDrawArrays(GL_QUADS, 0, XDIM * YDIM * ZDIM * 4);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);

	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);

	glPopAttrib();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	//if we haven't yet created an impostor texture, well, create one
	//anyway, copy from the framebuffer to the impostor texture
	//glCopyTexSubImage2D is faster so we use that if we're reloading the impostor

	if (glIsTexture(Cloud->ImpostorTex) == GL_FALSE) {
		glGenTextures(1, &Cloud->ImpostorTex);
		glBindTexture(GL_TEXTURE_2D, Cloud->ImpostorTex);
		glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 0, 0, ViewportSize, ViewportSize, 0);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	} else {
		glBindTexture(GL_TEXTURE_2D, Cloud->ImpostorTex);
		glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, ViewportSize, ViewportSize);
	}

	Cloud->ImpostorSize = ViewportSize;
}

void VolumetricClouds::LightCloud(VolumetricCloud* Cloud, Vector3 Sun) {
	//	the sun is a point
	Sun = Normalize(Sun) * Cloud->Radius * 1.5f + Cloud->Center;

	unsigned j;

	float d = Dist(Sun, Cloud->Center);
	float r = Cloud->Radius;
	float pr = 2.0f;

	// we setup our projection
	// The volume is a box, which will fit our cloud
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(-Cloud->Radius - pr, Cloud->Radius + pr, -Cloud->Radius - pr, Cloud->Radius + pr, d - r, d + r);

	// Change the camera to look at the cloud center from the sun
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	gluLookAt(Sun.x, Sun.y, Sun.z, Cloud->Center.x, Cloud->Center.y, Cloud->Center.z, 0, 1, 0);

	glPushAttrib(GL_VIEWPORT_BIT);
	glViewport(0, 0, SplatBufferSize, SplatBufferSize);

	// clear buffer
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// extract our vectors here
	float mat[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, mat);

	Vector3 vx(mat[0], mat[4], mat[8]);
	Vector3 vy(mat[1], mat[5], mat[9]);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, PuffTexture);

	float SolidAngle = 0.09f, Area;
	unsigned Pixels;
	double mp[16], mm[16];
	int vp[4];
	float *buf, avg;

	// setup projection
	glGetDoublev(GL_MODELVIEW_MATRIX, mm);
	glGetDoublev(GL_PROJECTION_MATRIX, mp);
	glGetIntegerv(GL_VIEWPORT, vp);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, PuffTexture);

	// here we set our blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0);

	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	Color4 LightColor(1.0f, 1.0f, 1.0f, 1.0f), ParticleColor;
	float factor = 1.0f;
	int ReadX, ReadY;

	Vector3 Light = Normalize(Sun);

	factor = SolidAngle / (4 * PI);

	for (int z = 0; z < ZDIM; z++) {
		for (int y = 0; y < YDIM; y++) {
			for (int x = 0; x < XDIM; x++) {
				double CenterX, CenterY, CenterZ;
				Vector3 pos = Vector3(x, y, z) + Cloud->Center;
				gluProject(pos.x,
					pos.y,
					pos.z,
					mm, mp, vp, &CenterX, &CenterY, &CenterZ);

				Area = SqDist(Sun, pos) * SolidAngle;
				Pixels = (int)(sqrt(Area) * SplatBufferSize / (2 * Cloud->Radius));
				if (Pixels < 1) Pixels = 1;

				// ensure we do not read outside the viewport
				ReadX = (int)(CenterX - Pixels / 2);
				if (ReadX < 0) ReadX = 0;
				ReadY = (int)(CenterY - Pixels / 2);
				if (ReadY < 0) ReadY = 0;

				buf = new float[Pixels * Pixels];

				glReadBuffer(GL_BACK);
				glReadPixels(ReadX, ReadY, Pixels, Pixels, GL_RED, GL_FLOAT, buf);

				avg = 0.0f;
				for (j = 0; j < Pixels * Pixels; j++) avg += buf[j];
				avg /= (Pixels * Pixels);

				delete[] buf;

				ParticleColor.R = LightColor.R * Albedo * Extinction * avg * factor;
				ParticleColor.G = LightColor.G * Albedo * Extinction * avg * factor;
				ParticleColor.B = LightColor.B * Albedo * Extinction * avg * factor;
				ParticleColor.A = 1.0f - exp(-Extinction);

				Cloud->clr[x][y][z] = ParticleColor;
				Cloud->clr[x][y][z].Clamp();


				// the phase function for color
				ParticleColor = ParticleColor * 1.5f;
				ParticleColor.Clamp();

				glColor4fv(!ParticleColor);

				glBegin(GL_QUADS);

				glTexCoord2f(1.0f, 0.0f);
				glVertex3fv(!(pos + (vx + vy) * -1.0f));
				glTexCoord2f(0.0f, 0.0f);
				glVertex3fv(!(pos + (vx - vy) *  1.0f));
				glTexCoord2f(0.0f, 1.0f);
				glVertex3fv(!(pos + (vx + vy) *  1.0f));
				glTexCoord2f(1.0f, 1.0f);
				glVertex3fv(!(pos + (vy - vx) *  1.0f));

				glEnd();
			}
		}
	}


	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);

	glPopAttrib();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

void VolumetricClouds::RenderCloudBillboard(VolumetricCloud* Cloud, float alpha) {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, Cloud->ImpostorTex);

	glEnable(GL_BLEND);
	glEnable(GL_ALPHA_TEST);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	glAlphaFunc(GL_GREATER, 0.0f);

	// Adjust texture coordinates to get rid of hard edges
	float texcoord = (float)(Cloud->ImpostorSize) / (float)ImpostorSize - 0.001f;

	// We make clouds transparent based on alpha value
	// we use alpha for everything because blending is enabled
	glColor4f(alpha, alpha, alpha, alpha);

	glBegin(GL_QUADS);

	glTexCoord2f(0.0f, 0.0f);
	glVertex3fv(!(Cloud->Center + (Cloud->vx + Cloud->vy) * -Cloud->Radius));

	glTexCoord2f(texcoord, 0.0f);
	glVertex3fv(!(Cloud->Center + (Cloud->vx - Cloud->vy) *  Cloud->Radius));

	glTexCoord2f(texcoord, texcoord);
	glVertex3fv(!(Cloud->Center + (Cloud->vx + Cloud->vy) *  Cloud->Radius));

	glTexCoord2f(0.0f, texcoord);
	glVertex3fv(!(Cloud->Center + (Cloud->vy - Cloud->vx) *  Cloud->Radius));

	glEnd();

	glDisable(GL_BLEND);
	glDisable(GL_ALPHA_TEST);

	glDisable(GL_TEXTURE_2D);
}

void VolumetricClouds::Destroy() {
	unsigned i;
	for (i = 0; i < Clouds.size(); i++)
	{
		delete[] Clouds[i].VertexBuffer;
		delete[] Clouds[i].ColorBuffer;
		delete[] Clouds[i].TexCoordBuffer;
	}

	Clouds.clear();
}
