#ifndef _VOLUMETRIC_H
#define _VOLUMETRIC_H

#define SORT_TOWARD 0
#define SORT_AWAY 1
#define XDIM 16
#define YDIM 16
#define ZDIM 32

struct VolumetricCloud {
	int			hum[XDIM][YDIM];
	int			act[XDIM][YDIM];
	int			cld[XDIM][YDIM];
	Color4		clr[XDIM][YDIM][ZDIM];
	unsigned 	ImpostorTex;
	Vector3		BoundingBox1, BoundingBox2;
	Vector3 	Center;
	float		Radius;
	Vector3*	VertexBuffer;
	Color4*		ColorBuffer;
	Vector2*	TexCoordBuffer;
	
	Vector3		LastCamera;
	Vector3		LastLight;
	float		DistanceFromCamera;
	int			ImpostorSize;
	Vector3		vx, vy;	//up and right vectors for impostor rendering
};

class VolumetricClouds {
public:
	VolumetricClouds();
	
	int 	Create(int NumClouds, float PlaneSize, float PlaneHeight);	
	void	Update(Vector3 Sun, Vector3 Camera);
	void	Render(Vector3 Sun, Vector3 Camera);
	void	Transition(Vector3 Sun, Vector3 Camera);
	void	Destroy();

private:

	void	RenderCloudBillboard(VolumetricCloud* Cloud, float alpha); 
	void	MakeCloudBillboard(VolumetricCloud* Cloud, Vector3 Sun, Vector3 Camera);
	
	void	LightCloud(VolumetricCloud* Cloud, Vector3 Sun);
	void	GrowCloud(VolumetricCloud* Cloud);

	void	GenerateTexture();

	unsigned PuffTexture;
	unsigned PuffImage;

	vector<VolumetricCloud> Clouds;
	
	int	SplatBufferSize;
	int ImpostorSize;
	float Albedo, Extinction;
};

#endif
